import './App.css';
import img1 from './assets/Grupo1.png';
import img2 from './assets/Grupo2.png';
import img3 from './assets/Figure_6.png';
import ReactFlagsSelect from 'react-flags-select';
import { useState, useCallback, useRef } from 'react';
import { toPng } from 'html-to-image';
function App() {
  const [selectedMatch, setSelectedMatch] = useState('BO1');
  const [name1, setName1] = useState('');
  const [name2, setName2] = useState('');
  const [score1, setScore1] = useState(0);
  const [score2, setScore2] = useState(0);
  const [selectedFlag1, setSelectedFlag1] = useState('VN');
  const [selectedFlag2, setSelectedFlag2] = useState('VN');
  const elementRef = useRef(null);
  const onButtonClick = useCallback(() => {
    if (elementRef.current === null) {
      return;
    }

    toPng(elementRef.current, { cacheBust: true })
      .then((dataUrl) => {
        const link = document.createElement('a');
        link.download = 'my-image-name.png';
        link.href = dataUrl;
        link.click();
      })
      .catch((err) => {
        console.log(err);
      });
  }, [elementRef]);
  return (
    <>
      <div ref={elementRef}>
        <div className="index">
          <div className="overlap-group-wrapper">
            <div className="overlap-group">
              <img className="figure" alt="Figure" src={img3} />
              <img className="grupo" alt="Grupo" src={img2} />
              <img className="img" alt="Grupo" src={img1} />
            </div>
          </div>
        </div>
        <p className="match">{selectedMatch}</p>
        <p className="score1">{score1}</p>
        <p className="score2">{score2}</p>
        <p className="name1">{name1}</p>
        <p className="name2">{name2}</p>
        {/* <ReactCountryFlag
          svg
          countryCode={selectedFlag1}
          style={{
            width: '81px',
            height: '81px',
            position: 'absolute',
            zIndex: '99',
            top: '87px',
            left: '122px',
          }}
          Searchable={true}
          aria-label="United States"
        />
        <ReactCountryFlag
          svg
          countryCode={selectedFlag2}
          style={{
            width: '81px',
            height: '81px',
            position: 'absolute',
            zIndex: '99',
            top: '88px',
            left: '432px',
          }}
          Searchable
          aria-label="United States"
        /> */}
        <img
          src={`https://flagcdn.com/h60/${selectedFlag1.toLowerCase()}.png`}
          alt=""
          style={{
            position: 'absolute',
            zIndex: '99',
            top: '65px',
            left: '122px',
          }}
        />
        <img
          src={`https://flagcdn.com/h60/${selectedFlag2.toLowerCase()}.png`}
          alt=""
          style={{
            position: 'absolute',
            zIndex: '99',
            top: '65px',
            left: '415px',
          }}
        />
      </div>
      <button onClick={onButtonClick} style={{ marginBottom: '10px' }}>
        Tải xuống ảnh
      </button>
      <div className="score">
        <p htmlFor="" className="label">
          Điểm đội 1
        </p>
        <input
          type="text"
          className="input1"
          onChange={(e) => setScore1(e.target.value)}
        />
        <p htmlFor="" className="label">
          Điểm đội 2
        </p>
        <input
          type="text"
          className="input2"
          onChange={(e) => setScore2(e.target.value)}
        />
      </div>
      <div className="match-number">
        <p htmlFor="" className="label">
          Số trận đấu
        </p>
        <select
          value={selectedMatch}
          onChange={(e) => setSelectedMatch(e.target.value)}
        >
          <option value="BO1">BO1</option>
          <option value="BO3">BO3</option>
          <option value="BO5">BO5</option>
          <option value="BO7">BO7</option>
        </select>
      </div>
      <div className="select-flag">
        <p htmlFor="" className="label">
          Cờ đội 1
        </p>
        <ReactFlagsSelect
          selected={selectedFlag1}
          onSelect={(code) => setSelectedFlag1(code)}
          fullWidth={false}
          searchable={true}
        />
        <p htmlFor="" className="label">
          Cờ đội 2
        </p>
        <ReactFlagsSelect
          selected={selectedFlag2}
          onSelect={(code) => setSelectedFlag2(code)}
          fullWidth={false}
          searchable={true}
        />
      </div>
      <div className="name-team">
        <p htmlFor="" className="label">
          Tên đội 1
        </p>
        <input
          type="text"
          className="input2"
          onChange={(e) => setName1(e.target.value)}
        />
        <p htmlFor="" className="label">
          Tên đội 1
        </p>
        <input
          type="text"
          className="input2"
          onChange={(e) => setName2(e.target.value)}
        />
      </div>
    </>
  );
}

export default App;
